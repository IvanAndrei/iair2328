package tasks.integration;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.model.Task;
import tasks.model.TaskStubMock;
import tasks.repository.ArrayTaskList;
import tasks.services.TasksService;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class TasksServiceIntegrationTest {

    private ArrayTaskList taskList;
    private TasksService tasksService;

    private  SimpleDateFormat dateFormat ;

    @BeforeEach
    void setUp() {
        taskList =new ArrayTaskList();
        tasksService = new TasksService(taskList);
        dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm");
    }

    @Test
    public void test_integration_get_add(){
        try{
        Task t = new Task("T", dateFormat.parse("2020-03-12 14:00"));
        taskList.add(t);
        ObservableList<Task> obs = FXCollections.observableArrayList();
        obs=tasksService.getObservableList();
        assertEquals(1,obs.size());
        assertTrue(obs.get(0).getTitle().equals("T"));}
        catch (Exception e){
            e.printStackTrace();
        }
    }

@Test
    public void test_filter_tasks_integration() {
    try {
        Date d1 = dateFormat.parse("2020-09-23 13:00");
        Date d2 = dateFormat.parse("2020-11-23 13:00");
        Task t = new Task("t1", dateFormat.parse("2020-10-23 13:00"));
        Task tt = new Task("t2", dateFormat.parse("2020-10-23 14:00"));
        taskList.add(t);
        t.setActive(true);
        taskList.add(tt);
        Iterable<Task> filtered = tasksService.filterTasks(d1, d2);
        assertEquals(1,filtered.spliterator().getExactSizeIfKnown());
    } catch (Exception e) {
        e.printStackTrace();
    }
}

}