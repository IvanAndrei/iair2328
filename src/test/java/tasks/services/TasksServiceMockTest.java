package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.model.Task;
import tasks.repository.ArrayTaskList;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TasksServiceMockTest {
    private ArrayTaskList tasksList;
    private TasksService tasksService;
    private SimpleDateFormat dateFormat;

    @BeforeEach
    public void setUp() {
        tasksList = mock(ArrayTaskList.class);
        tasksService = new TasksService(tasksList);
        try {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_get_tasks() {
      //  TasksService spyTasksService = tasksService;

        ObservableList<Task> obs = FXCollections.observableArrayList();
        try {
            Task t = new Task("t1", dateFormat.parse("2020-10-23 13:00"));
            Task tt = new Task("t2", dateFormat.parse("2020-10-23 14:00"));
            when(tasksList.getAll()).thenReturn(Arrays.asList(t,tt));
        } catch (Exception e) {
            e.printStackTrace();
        }
        obs = tasksService.getObservableList();
        assertEquals(2,obs.size());
        assertTrue(obs.get(0).getTitle().equals("t1"));

    }

    @Test

    public void test_filter_tasks_unit() {
        try {
            Date d1 = dateFormat.parse("2020-09-23 13:00");
            Date d2 = dateFormat.parse("2020-11-23 13:00");
            Task t = new Task("t1", dateFormat.parse("2020-10-23 13:00"));
            Task tt = new Task("t2", dateFormat.parse("2020-10-23 14:00"));
            when(tasksList.getAll()).thenReturn(Arrays.asList(t, tt));
            Iterable<Task> filtered = tasksService.filterTasks(d1, d2);
            assertNotNull(filtered);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Test

    public void test_form_time_unit(){
        int time = 40;
        String s=tasksService.formTimeUnit(time);
        assertTrue(s.equals("40"));
    }
}