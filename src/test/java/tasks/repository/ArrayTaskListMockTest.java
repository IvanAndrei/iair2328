package tasks.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import tasks.model.Task;
import tasks.model.TaskStubMock;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.DataFormatException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

class ArrayTaskListMockTest {
    private ArrayTaskList tasksTest ;
    private SimpleDateFormat dateFormat;
    @BeforeEach
    public void setUp() {
        tasksTest=new ArrayTaskList();
        try{
            dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAddValid(){
      //  ArrayTaskList spyTasksTest=spy(tasksTest);
//        try {
//            Task t = new Task("t1", dateFormat.parse("2020-10-23 13:00"));
//            spyTasksTest.add(t);
//
//        }catch (Exception e){
//            e.getMessage();
//        }
        Task t1=Mockito.mock(Task.class);
        tasksTest.add(t1);
        assertEquals(1,tasksTest.size());
        Task t2=Mockito.mock(Task.class);
        tasksTest.add(t2);
        assertEquals(2,tasksTest.size());
    }
    @Test
    public void test_getAll_valid(){
        Task t1=Mockito.mock(Task.class);
        tasksTest.add(t1);
        Task t2=Mockito.mock(Task.class);
        tasksTest.add(t2);
        assertEquals(2,tasksTest.size());
        List<Task> arrayList= tasksTest.getAll();
        assertEquals(2,arrayList.size());
    }



}